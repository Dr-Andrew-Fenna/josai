package josai.josai;

/**
 * I am JOSAI!
 * ...Java Open Source Artificial Intelligence
 * ...start date 04/03/2019
 * ...map the human brain into basic modules of neural networks
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "I am JOSAI!" );
    }
}
